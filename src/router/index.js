import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import TokenService from '@/services/token.service'
import store from '@/store'

const requireAuth = function (to, from, next) {
  if (!TokenService.hasToken()) {
    next({ name: 'home', query: { redirect: to.fullPath } })
  } else {
    next()
  }
}

const requireGuest = function (to, from, next) {
  if (TokenService.hasToken()) {
    next({ name: 'dashboard' })
  } else {
    next()
  }
}

const logout = function (to, from, next) {
  TokenService.removeToken()
  store.dispatch('logout')
  next({ name: 'home' })
}

Vue.use(VueRouter)

const routes = [
  { path: '/uitloggen', name: 'logout', beforeEnter: logout },
  { path: '/', name: 'home', component: Home, beforeEnter: requireGuest },
  { path: '/dashboard', name: 'dashboard', component: () => import('../views/Dashboard.vue'), beforeEnter: requireAuth },
  { path: '/member/:id', name: 'member', component: () => import('../views/Member.vue'), beforeEnter: requireAuth },
  { path: '/admin/:id', name: 'admin', component: () => import('../views/AdminProfile.vue'), beforeEnter: requireAuth }
]

const router = new VueRouter({
  mode: 'history',
  // base: process.env.BASE_URL,
  routes
})

export default router
