import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import { preset } from 'vue-cli-plugin-vuetify-preset-reply/preset'

Vue.use(Vuetify)

export default new Vuetify({
  preset,
  colors: {
    primary: '#344955',
    secondary: '#232F34',
    accent: '#4A6572',
    error: '#FF5252',
    info: '#F9AA33',
    success: '#4CAF50',
    warning: '#FB8C00'
  }
})
