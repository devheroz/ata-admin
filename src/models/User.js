const User = {
  createdAt: '',
  firstname: '',
  lastname: '',
  birthDate: '',
  city: '',
  address: '',
  postalCode: '',
  phoneNumber: '',
  email: '',
  startDate: '',
  danceCourse: '',
  singCourse: '',
  paymentFoundation: '',
  age: '',
  amount: '', // prijs van cursus
  discount: false,
  if: false,
  familyDiscount: false,
  package: '',
  discountType: '',
  discountAmount: '',
  commantary: '',
  payment: {
    fullname: '',
    automaticPaymentDate: '',
    iban: '',
    paymentAddress: '',
    city: '',
    paymentPostalCode: '',
    paymentEmail: '',
    signature: {
      data: ''
    }
  },
  paymentMonth:
    {
      Jan: 'Niet Betaald',
      Feb: 'Niet Betaald',
      Maa: 'Niet Betaald',
      Apr: 'Niet Betaald',
      Mei: 'Niet Betaald',
      Jun: 'Niet Betaald',
      Jul: 'Niet Betaald',
      Aug: 'Niet Betaald',
      Sep: 'Niet Betaald',
      Okt: 'Niet Betaald',
      Nov: 'Niet Betaald',
      Dec: 'Niet Betaald'
    }
}

export default User
