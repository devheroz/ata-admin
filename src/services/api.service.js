/*eslint-disable */
import axios from 'axios'
import TokenService from '@/services/token.service'
import ConfigService from '@/services/config.service'
import qs from 'qs'
import router from '../router'

axios.defaults.baseURL = ConfigService.apiUrl


axios.interceptors.request.use(function(config) {
    let accessToken = TokenService.getToken()

    if (accessToken) {
        config.headers['Authorization'] = `${accessToken}`;
    }

    config.headers['Content-Type'] = 'application/json';
    NProgress.start()
    return config

}, function(error) {
    return Promise.reject(error)
})


axios.interceptors.response.use(function(response) {
    NProgress.done()
    return response
}, function(error) {

    if (error.response.status === 401) {
        TokenService.removeToken()
    }
   
    return Promise.reject(error)
})

function mapOptions (options) {
    let convertedOptions = {
      pageNo: options && options.page ? options.page : null,
      'size': options && options.itemsPerPage ? options.itemsPerPage : null
    }
    if (options && options.sortBy[0]) {
      convertedOptions.sort = (options && options.sortDesc[0] ? '-' : '') + options.sortBy[0]
    }
    
    return convertedOptions
}

const ApiService = {

    login(data) {
        return new Promise((resolve, reject) => {
            axios.post('/admin-login', data)
                .then(response => {
                    let data = response.data;
                    let accessToken = data.accessToken || data.token || null
                    if (accessToken) {
                        TokenService.setToken(accessToken)
                        resolve(response.data);
                    } else {
                        reject();
                    }
                })
                .catch(error => {
                    reject(error)
                })
        })
    },

    allusers(){
        return new Promise((resolve, reject) => {
            axios.get('/users').then(response => {
                resolve(response.data)
            }).catch(error => {
                reject(error)
            })
        })
    },

    getUsers(options = null, config = {}){
        return new Promise((resolve, reject) => {
            config.params = Object.assign({}, (config.params || {}), mapOptions(options))
            config.paramsSerializer = params => qs.stringify(params, { arrayFormat: 'brackets' })
            axios.get(`/allusers?pageNo=${config.params.pageNo}&size=${config.params.size}`).then(({ data }) => {
              let total = data.totalDocs
              resolve({ items: data.docs, total, config })
            }).catch(error => {
              reject(error)
            })
          })
    },

    getAllDancers(options = null, config = {}){
        return new Promise((resolve, reject) => {
            config.params = Object.assign({}, (config.params || {}), mapOptions(options))
            config.paramsSerializer = params => qs.stringify(params, { arrayFormat: 'brackets' })
            axios.get(`/dance/alldancers?pageNo=${config.params.pageNo}&size=${config.params.size}`).then(({ data }) => {
              let total = data.totalDocs
              resolve({ items: data.docs, total, config })
            }).catch(error => {
              reject(error)
            })
          })
    },


    getHiphopKids(options = null, config = {}){
        return new Promise((resolve, reject) => {
            config.params = Object.assign({}, (config.params || {}), mapOptions(options))
            config.paramsSerializer = params => qs.stringify(params, { arrayFormat: 'brackets' })
            axios.get(`/dance/hiphopkids?pageNo=${config.params.pageNo}&size=${config.params.size}`).then(({ data }) => {
              let total = data.totalDocs
              console.log(data)
              resolve({ items: data.docs, total, config })
            }).catch(error => {
              reject(error)
            })
          })
    },

    getHiphopKidsPartTwo(options = null, config = {}){
        return new Promise((resolve, reject) => {
            config.params = Object.assign({}, (config.params || {}), mapOptions(options))
            config.paramsSerializer = params => qs.stringify(params, { arrayFormat: 'brackets' })
            axios.get(`/dance/hiphopkidspartwo?pageNo=${config.params.pageNo}&size=${config.params.size}`).then(({ data }) => {
              let total = data.totalDocs
              resolve({ items: data.docs, total, config })
            }).catch(error => {
              reject(error)
            })
          })
    },

    getHiphopJuniors(options = null, config = {}){
      return new Promise((resolve, reject) => {
          config.params = Object.assign({}, (config.params || {}), mapOptions(options))
          config.paramsSerializer = params => qs.stringify(params, { arrayFormat: 'brackets' })
          axios.get(`/dance/hiphopjuniors?pageNo=${config.params.pageNo}&size=${config.params.size}`).then(({ data }) => {
            let total = data.totalDocs
            resolve({ items: data.docs, total, config })
          }).catch(error => {
            reject(error)
          })
        })
  },

  getHiphopAdults(options = null, config = {}){
    return new Promise((resolve, reject) => {
        config.params = Object.assign({}, (config.params || {}), mapOptions(options))
        config.paramsSerializer = params => qs.stringify(params, { arrayFormat: 'brackets' })
        axios.get(`/dance/hiphopadults?pageNo=${config.params.pageNo}&size=${config.params.size}`).then(({ data }) => {
          let total = data.totalDocs
          resolve({ items: data.docs, total, config })
        }).catch(error => {
          reject(error)
        })
      })
},

getHiphopLadies(options = null, config = {}){
  return new Promise((resolve, reject) => {
      config.params = Object.assign({}, (config.params || {}), mapOptions(options))
      config.paramsSerializer = params => qs.stringify(params, { arrayFormat: 'brackets' })
      axios.get(`/dance/hiphopladies?pageNo=${config.params.pageNo}&size=${config.params.size}`).then(({ data }) => {
        let total = data.totalDocs
        resolve({ items: data.docs, total, config })
      }).catch(error => {
        reject(error)
      })
    })
},

getHiphopSelection(options = null, config = {}){
  return new Promise((resolve, reject) => {
      config.params = Object.assign({}, (config.params || {}), mapOptions(options))
      config.paramsSerializer = params => qs.stringify(params, { arrayFormat: 'brackets' })
      axios.get(`/dance/selectionhiphop?pageNo=${config.params.pageNo}&size=${config.params.size}`).then(({ data }) => {
        let total = data.totalDocs
        resolve({ items: data.docs, total, config })
      }).catch(error => {
        reject(error)
      })
    })
},

getHiphopSelectionKids(options = null, config = {}){
  return new Promise((resolve, reject) => {
      config.params = Object.assign({}, (config.params || {}), mapOptions(options))
      config.paramsSerializer = params => qs.stringify(params, { arrayFormat: 'brackets' })
      axios.get(`/dance/selectionhiphopkids?pageNo=${config.params.pageNo}&size=${config.params.size}`).then(({ data }) => {
        let total = data.totalDocs
        resolve({ items: data.docs, total, config })
      }).catch(error => {
        reject(error)
      })
    })
},

getBreakdanceKids(options = null, config = {}){
  return new Promise((resolve, reject) => {
      config.params = Object.assign({}, (config.params || {}), mapOptions(options))
      config.paramsSerializer = params => qs.stringify(params, { arrayFormat: 'brackets' })
      axios.get(`/dance/breakdancekids?pageNo=${config.params.pageNo}&size=${config.params.size}`).then(({ data }) => {
        let total = data.totalDocs
        resolve({ items: data.docs, total, config })
      }).catch(error => {
        reject(error)
      })
    })
},

getBreakdanceTeens(options = null, config = {}){
  return new Promise((resolve, reject) => {
      config.params = Object.assign({}, (config.params || {}), mapOptions(options))
      config.paramsSerializer = params => qs.stringify(params, { arrayFormat: 'brackets' })
      axios.get(`/dance/breakdanceteens?pageNo=${config.params.pageNo}&size=${config.params.size}`).then(({ data }) => {
        let total = data.totalDocs
        resolve({ items: data.docs, total, config })
      }).catch(error => {
        reject(error)
      })
    })
},

getDancehall(options = null, config = {}){
  return new Promise((resolve, reject) => {
      config.params = Object.assign({}, (config.params || {}), mapOptions(options))
      config.paramsSerializer = params => qs.stringify(params, { arrayFormat: 'brackets' })
      axios.get(`/dance/dancehall?pageNo=${config.params.pageNo}&size=${config.params.size}`).then(({ data }) => {
        let total = data.totalDocs
        resolve({ items: data.docs, total, config })
      }).catch(error => {
        reject(error)
      })
    })
},

getHeels(options = null, config = {}){
  return new Promise((resolve, reject) => {
      config.params = Object.assign({}, (config.params || {}), mapOptions(options))
      config.paramsSerializer = params => qs.stringify(params, { arrayFormat: 'brackets' })
      axios.get(`/dance/heels?pageNo=${config.params.pageNo}&size=${config.params.size}`).then(({ data }) => {
        let total = data.totalDocs
        resolve({ items: data.docs, total, config })
      }).catch(error => {
        reject(error)
      })
    })
},

addNewMember (data) {
  return new Promise((resolve, reject) => {
    axios.post('/admin/create-user', data).then(response => {
      resolve(response)
    }).catch(error => {
      reject(error)
    })
  })
},

getSingleUser() {
  let userId = router.history.current.params.id;
  return new Promise((resolve, reject) => {
    axios.get(`/user/${userId}`).then(response => {
      resolve(response)
    }).catch(error => {
      reject(error)
    })
  })
},

getAdmin() {
  let userId = router.history.current.params.id;
  return new Promise((resolve, reject) => {
    axios.get(`/get-profile/${userId}`).then(response => {
      resolve(response)
    }).catch(error => {
      reject(error)
    })
  })
},

editUser(data) {
  let userId = router.history.current.params.id;
  return new Promise((resolve, reject) => {
    axios.put('/edit-personal-info/' + userId, data).then(response => {
      resolve(response)
    }).catch(error => {
      reject(error)
    })
  })
},

editCourseInfo(data) {
  let userId = router.history.current.params.id;
  return new Promise((resolve, reject) => {
    axios.put('/edit-course-info/' + userId, data).then(response => {
      resolve(response)
    }).catch(error => {
      reject(error)
    })
  })
},

editPayment(data) {
  let userId = router.history.current.params.id;
  return new Promise((resolve, reject) => {
    axios.put(`/edit-course-info/${userId}`, data).then(response => {
      resolve(response)
    }).catch(error => {
      reject(error)
    })
  })
},

getLoginUser() {
  return new Promise((resolve, reject) => {
    axios.get(`/admin-profile`).then(response => {
      resolve(response)
    }).catch(error => {
      reject(error)
    })
  })
}

}

export default ApiService
